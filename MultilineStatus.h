/*
 * AsciiStatus -- Dynamic progress displays for an ASCII terminal
 * 
 * Copyright (C) 2009, 2010 Mathew Cucuzella (kookjr@gmail.com)
 *
 * This file is part of AsciiStatus.
 *
 * AsciiStatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AsciiStatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AsciiStatus.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MULTILINESTATUS_H
#define MULTILINESTATUS_H

#include <string>
#include <vector>
#include <pthread.h>

namespace mls {

    class ProgressBarData;
    class MarqueeData;

    class MultilineStatus {
    public:
        MultilineStatus(int num_lines, int line_width=0, bool debug=false);
        ~MultilineStatus(void);

        void addMarqueeLine(int n, const std::string& name, bool with_count=false);
        void addProgressBarLine(int n, const std::string& name);

        void addMarqueeElement(int n, const std::string& el, int pos=0);
        void remMarqueeElement(int n, int pos=-1);
        void remMarqueeElement(int n, const std::string& el);
        void setProgress(int n, double percent, const std::string source);

        void increaseLines(int num_lines);

    private:
        void gotoLine(int n);

        pthread_mutex_t mLock;
        int mWidth;
        bool mDebug;

        std::vector<MarqueeData*> mMarqueeDatas;
        std::vector<ProgressBarData*> mProgressBarDatas;
    };

}

#endif /* MULTILINESTATUS_H */

/* Local Variables: */
/* mode: C++ */
/* End: */
