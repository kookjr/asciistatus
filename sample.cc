/*
 * AsciiStatus -- Dynamic progress displays for an ASCII terminal
 * 
 * Copyright (C) 2009, 2010 Mathew Cucuzella (kookjr@gmail.com)
 *
 * This file is part of AsciiStatus.
 *
 * AsciiStatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AsciiStatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AsciiStatus.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <stdio.h>

#include "ProgressBar.h"
#include "Marquee.h"
#include "ProgressBarData.h"
#include "MarqueeData.h"
#include "MultilineStatus.h"

static const char *elements[] = {
     "EMix",
     "ReGen",
     "GBTV",
     "TIG",
     "RubyFruit",
     "PhonesShow",
     "AAS",
     "ScissorKick",
     "FreeBuddhistAudio",
     "AudioDharma",
     "LinuxOutlaws",
     "UbuntuUK",
     "CellPhoneJunkie",
     "SFLShow",
     "SERadio",
     "RubyOnRails",
     "RealDeal",
     "MacCast",
     "ITConversations",
     "HDTV",
     "Hanselminutes",
     "Freenode",
     "DialedIn",
     "ruby5",
     "ELF",
     "AAS_audio",
     "PhonesChat",
     0
};

#define MAXWIDTH 80

int main(int argc, char** argv) {

    if (argc != 2) {
        fprintf(stderr, "Usage: %s { m | d | p | c | a }\n", argv[0]);
        return 1;
    }

    if (argv[1][0] == 'm' || argv[1][0] == 'a') {
        // stand-alone Marquee
        mls::MarqueeData* md  = new mls::MarqueeData("Feeds", MAXWIDTH);
        mls::Marquee* marquee = new mls::Marquee();

        for (int i=0; elements[i]; i++) {
            md->addElement(elements[i]);
            marquee->update(*md);
            usleep(200000);
        }

        delete md;
        delete marquee;

        printf("\n\n");
    }
    if (argv[1][0] == 'd' || argv[1][0] == 'a') {
        // stand-alone Marquee
        mls::MarqueeData* md  = new mls::MarqueeData("CntFeeds", MAXWIDTH, true);
        mls::Marquee* marquee = new mls::Marquee();

        for (int i=0; elements[i]; i++) {
            md->addElement(elements[i]);
            marquee->update(*md);
            usleep(200000);
        }

        delete md;
        delete marquee;

        printf("\n\n");
    }
    if (argv[1][0] == 'p' || argv[1][0] == 'a') {
        // stand-alone Progress Bar
        mls::ProgressBarData* pbd = new mls::ProgressBarData("Thread 1", MAXWIDTH);
        mls::ProgressBar* pbar    = new mls::ProgressBar();

        pbd->setSource("AAS");
        for (int i=0; i <= 10; i++) {
            pbd->setProgress(0.10 * (double )i);
            pbar->update(*pbd);
            usleep(200000);
        }
        delete pbd;
        delete pbar;

        printf("\n\n");
    }
    if (argv[1][0] == 'c' || argv[1][0] == 'a') {
        // combined Marquees and Progress Bars
        mls::MultilineStatus* status = new mls::MultilineStatus(5);
        status->addMarqueeLine(1, "   Check");
        status->addMarqueeLine(2, "    Skip");
        status->addMarqueeLine(3, "Queue", true);
        status->addProgressBarLine(4, "Thread 6");
        status->addProgressBarLine(5, "Thread 7");

        // add data to all lines
        for (int i=0; elements[i]; i++) {
            status->addMarqueeElement(1, elements[i]);
            status->addMarqueeElement(2, elements[i]);
            status->addMarqueeElement(3, elements[i], -1);
            status->setProgress(4, (double )i * 0.10, elements[0]);
            status->setProgress(5, (double )i * 0.10, elements[7]);
            usleep(200000);
        }
        // remove Queue items from front to back
        for (int i=0; elements[i]; i++) {
            status->remMarqueeElement(3, 0);
            usleep(200000);
        }
        // remove even Check items
        for (int i=0; i < sizeof(elements)/sizeof(char *); i += 2) {
            if (elements[i]) {
                status->remMarqueeElement(1, elements[i]);
                usleep(200000);
            }
        }
        // remove odd Check items
        for (int i=1; i < sizeof(elements)/sizeof(char *); i += 2) {
            if (elements[i]) {
                status->remMarqueeElement(1, elements[i]);
                usleep(200000);
            }
        }

        delete status;

        printf("\n\n");
    }

    return 0;
}
