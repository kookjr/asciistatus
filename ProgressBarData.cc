/*
 * AsciiStatus -- Dynamic progress displays for an ASCII terminal
 * 
 * Copyright (C) 2009, 2010 Mathew Cucuzella (kookjr@gmail.com)
 *
 * This file is part of AsciiStatus.
 *
 * AsciiStatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AsciiStatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AsciiStatus.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "ProgressBarData.h"

namespace mls {

    ProgressBarData::ProgressBarData(const std::string& title, int width) :
        mTitle(title),
        mSource(""),
        mBuffer(""),
        mWidth(width),
        mMaxSourceWidth((int )(width * 0.15)),
        mPercent(0.0)
    {
    }

    ProgressBarData::~ProgressBarData(void) {
    }

    bool ProgressBarData::percentChange(double percent) {
        return (percent != mPercent);
    }

    void ProgressBarData::setSource(const std::string& source) {
        mSource = source;
    }

    void ProgressBarData::setProgress(double percent) {
        mPercent = percent;
    }

    std::string ProgressBarData::getDisplayLine(void) {
        int bar_size = mWidth - (
            mTitle.size() +           // title
            2 +                       // " ["
            2 +                       // "] "
            3 +                       // percent
            2 +                       // "% "
            mMaxSourceWidth);         // data source (allotted area)
        if (bar_size < 4) {
            return "screen width too small";
        }

        mBuffer = mTitle;
        mBuffer.append(" [");

        int progress = (int )((double )bar_size * mPercent);
        for (int i=0; i < bar_size; i++) {
            mBuffer.append( (i < progress) ? "=" : " ");
        }

        mBuffer.append("] ");

        char buf[5];
        int per = (int )(mPercent * 100);
        if (per > 100) {
            per = 100;
        }
        sprintf(buf, "%3d", per);
        mBuffer.append(buf);

        mBuffer.append("% ");
        mBuffer.append(mSource);

        int left = mWidth - mBuffer.size();
        // left may be negative, so loop would be skipped
        for ( ; left > 0; left--) {
            mBuffer.append(" ");
        }

        return mBuffer.substr(0, mWidth);
        //"Thread 1 [======>                    ]  28% AAS  ";
    }

}
