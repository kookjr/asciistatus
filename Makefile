# AsciiStatus -- Dynamic progress displays for an ASCII terminal
# 
# Copyright (C) 2009 Mathew Cucuzella (kookjr@gmail.com)
#
# This file is part of AsciiStatus.
#
# AsciiStatus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# AsciiStatus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with AsciiStatus.  If not, see <http://www.gnu.org/licenses/>.

PREFIX      = /usr
H_INST_OPTS = -o root -g root -m 644
L_INST_OPTS = -o root -g root -m 644

CPP     = g++
CPPFLAGS= -O -I.
#CPPFLAGS += -g  # debug
LDFLAGS = -L. -lncurses

LIBSRC  = Marquee.cc MarqueeData.cc MultilineStatus.cc ProgressBar.cc \
          ProgressBarData.cc
LIBOBJ  = $(LIBSRC:.cc=.o)
HEADERS = $(LIBSRC:.cc=.h)
LIB     = libastatus.a

all: sample libastatus.a

sample: sample.o $(LIB)
	g++ -o $@ $< -lastatus $(LDFLAGS)

libastatus.a: $(LIBOBJ)
	ar rs $(LIB) $(LIBOBJ)

clean:
	rm -f $(LIBOBJ) $(LIB) sample.o

clobber: clean
	rm -f sample

install: all
	for header in $(HEADERS); \
	do \
	    install $(H_INST_OPTS) $$header $(PREFIX)/include/$$header; \
	done
	install $(L_INST_OPTS) $(LIB) $(PREFIX)/lib/$(LIB)

uninstall:
	for header in $(HEADERS); \
	do \
	    rm -f $(PREFIX)/include/$$header; \
	done
	rm -f $(PREFIX)/lib/$(LIB)
