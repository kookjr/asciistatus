# AsciiStatus -- Dynamic progress displays for an ASCII terminal

Version 0.2

This C++ library provides a dynamic way to show progress on an
ASCII terminal.

The features include:

* Single line marquee, which can add elements to the beginning
  or the end, and remove them from either side as well.
* Single line titled progress bar with percent bar, percent
  value and source.
* Multiline status area consisting of any combination of the
  above, one per line. This interface is thread safe so it can
  be used, for example, from a multithreaded file downloader.
* A sample program demonstrating the use of each above type.
  The sample displays a mocked up interface for a podcast
  downloader.

## Installation

AsciiStatus is a library and must be installed from source.

Download and install the source in a directory, e.g. 'asciistatus'. Then build the source.

    cd asciistatus
    make
    make install

# Sample Usage

The following is a simple sample of the progress bar. See
the included sample.cc for other examples.

    // stand-alone Progress Bar
    mls::ProgressBarData* pbd = new mls::ProgressBarData("Download", 80);
    mls::ProgressBar* pbar    = new mls::ProgressBar();
    pbd->setSource("RubyFruit");

    for (int i=0; i <= 10; i++) {
        // step percent, 0.00, 0.10, 0.20, ...
        pbd->setProgress(0.10 * (double )i);
        pbar->update(*pbd);
        // pause for a short time for some realism
        usleep(200000);
    }

    delete pbd;
    delete pbar;

    // move to next line so shell prompt does not cover up
    // the final state of the progress bar
    printf("\n");

## Development
### Source Repository

AsciiStatus is currently hosted at gitlab. The gitlab web page is
``http://gitlab.com/kookjr/asciistatus``. The public git clone URL is

* ``https://gitlab.com/kookjr/asciistatus.git``

### Running the Sample

Build the code and run as follows

    make
    ./sample a

### Full Sample Usage

    Usage: sample { m | p | c | a }

Run the single line marquee status.

    sample m

![sample, -m option](http://gitlab.com/kookjr/asciistatus/raw/master/images/asciistatus_m_opt.png)

Run the single line progress bar.

    sample p

![sample, -m option](http://gitlab.com/kookjr/asciistatus/raw/master/images/asciistatus_p_opt.png)

Run the multiline status, using a combination of the two above.

    sample c

![sample, -m option](http://gitlab.com/kookjr/asciistatus/raw/master/images/asciistatus_c_opt.png)

Run all sample status screens.

    sample a

### Issues and Bug Reports

Report and follow issue status at the project's GitLab site.

* See Issues tab

## License

GNU LGPL, Lesser General Public License version 3.0. For details,
see files "COPYING" and "COPYING.LESSER".

# Changelog

v0.2 

* API change to allow removeing of elements from marquee by name
* add curses debug feature
* add install/uninstall targets to make

v0.1

* initial version
