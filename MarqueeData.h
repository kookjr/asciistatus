/*
 * AsciiStatus -- Dynamic progress displays for an ASCII terminal
 * 
 * Copyright (C) 2009, 2010 Mathew Cucuzella (kookjr@gmail.com)
 *
 * This file is part of AsciiStatus.
 *
 * AsciiStatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AsciiStatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AsciiStatus.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MARQUEEDATA_H
#define MARQUEEDATA_H

#include <string>
#include <deque>

namespace mls {

    class MarqueeData {
    public:
        // if add_title_count, add -%02d to end of title (3 chars)
        MarqueeData(const std::string& title, int width, bool add_title_cnt=false);
        ~MarqueeData(void);

        void addElement(const std::string& el, int pos=0);
        void remElement(int pos=0);
        void remElement(const std::string& el);

        std::string getDisplayLine(void);

    private:
        std::string mTitle;
        std::deque<std::string> mElements;
        int mWidth;
        bool mAddTitleCount;
    };

}

#endif /* MARQUEEDATA_H */

/* Local Variables: */
/* mode: C++ */
/* End: */
