/*
 * AsciiStatus -- Dynamic progress displays for an ASCII terminal
 * 
 * Copyright (C) 2009, 2010 Mathew Cucuzella (kookjr@gmail.com)
 *
 * This file is part of AsciiStatus.
 *
 * AsciiStatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AsciiStatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AsciiStatus.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ncurses.h"

#include "Marquee.h"
#include "MarqueeData.h"
#include "ProgressBar.h"
#include "ProgressBarData.h"
#include "MultilineStatus.h"

#define DEFAULT_WIDTH 80

namespace mls {
    MultilineStatus::MultilineStatus(int num_lines, int line_width, bool debug) :
        mWidth(line_width),
        mDebug(debug),
        mMarqueeDatas(num_lines),
        mProgressBarDatas(num_lines)
    {
        pthread_mutex_init(&mLock, NULL);

        if (! mDebug) {
            initscr();
            if (mWidth == 0) {
                mWidth = getmaxx(stdscr);
            }
        }
        else {
            if (mWidth == 0) {
                mWidth = DEFAULT_WIDTH;
            }
        }
    }

    MultilineStatus::~MultilineStatus(void) {
        int i;

        for (i=0; i < mMarqueeDatas.size(); i++) {
            if (mMarqueeDatas[i] != NULL) {
                delete mMarqueeDatas[i];
            }
        }

        for (i=0; i < mProgressBarDatas.size(); i++) {
            if (mProgressBarDatas[i] != NULL) {
                delete mProgressBarDatas[i];
            }
        }

        endwin();
    }

    void MultilineStatus::addMarqueeLine(int n, const std::string& name, bool with_count) {
        int idx = n - 1;

        if (idx < mMarqueeDatas.size()) {
            pthread_mutex_lock(&mLock);
            if (mMarqueeDatas[idx] == NULL) {
                mMarqueeDatas[idx] = new MarqueeData(name, mWidth, with_count);
                gotoLine(n);
                if (! mDebug)
                    printw("%s", mMarqueeDatas[idx]->getDisplayLine().c_str());
                else
                    printf("%s\n", mMarqueeDatas[idx]->getDisplayLine().c_str());
                refresh();
            }
            pthread_mutex_unlock(&mLock);
        }
    }

    void MultilineStatus::addProgressBarLine(int n, const std::string& name) {
        int idx = n - 1;

        if (idx < mProgressBarDatas.size()) {
            pthread_mutex_lock(&mLock);
            if (mProgressBarDatas[idx] == NULL) {
                mProgressBarDatas[idx] = new ProgressBarData(name, mWidth);
            }
            pthread_mutex_unlock(&mLock);
        }
    }

    void MultilineStatus::addMarqueeElement(int n, const std::string& el, int pos) {
        int idx = n - 1;
        if (idx < mMarqueeDatas.size()) {
            pthread_mutex_lock(&mLock);
            if (mMarqueeDatas[idx] != NULL) {
                mMarqueeDatas[idx]->addElement(el, pos);
                gotoLine(n);
                if (! mDebug)
                    printw("%s", mMarqueeDatas[idx]->getDisplayLine().c_str());
                else
                    printf("%s\n", mMarqueeDatas[idx]->getDisplayLine().c_str());
                refresh();
            }
            pthread_mutex_unlock(&mLock);
        }
    }

    void MultilineStatus::remMarqueeElement(int n, int pos) {
        int idx = n - 1;
        if (idx < mMarqueeDatas.size()) {
            pthread_mutex_lock(&mLock);
            if (mMarqueeDatas[idx] != NULL) {
                mMarqueeDatas[idx]->remElement(pos);
                gotoLine(n);
                if (! mDebug)
                    printw("%s", mMarqueeDatas[idx]->getDisplayLine().c_str());
                else
                    printf("%s\n", mMarqueeDatas[idx]->getDisplayLine().c_str());
                refresh();
            }
            pthread_mutex_unlock(&mLock);
        }
    }

    void MultilineStatus::remMarqueeElement(int n, const std::string& el) {
        int idx = n - 1;
        if (idx < mMarqueeDatas.size()) {
            pthread_mutex_lock(&mLock);
            if (mMarqueeDatas[idx] != NULL) {
                mMarqueeDatas[idx]->remElement(el);
                gotoLine(n);
                if (! mDebug)
                    printw("%s", mMarqueeDatas[idx]->getDisplayLine().c_str());
                else
                    printf("%s\n", mMarqueeDatas[idx]->getDisplayLine().c_str());
                refresh();
            }
            pthread_mutex_unlock(&mLock);
        }
    }

    void MultilineStatus::setProgress(int n, double percent, const std::string source) {
        int idx = n - 1;
        if (idx < mProgressBarDatas.size()) {
            pthread_mutex_lock(&mLock);
            if (mProgressBarDatas[idx] != NULL) {
                if (mProgressBarDatas[idx]->percentChange(percent)) {
                    mProgressBarDatas[idx]->setProgress(percent);
                    mProgressBarDatas[idx]->setSource(source);
                    gotoLine(n);
                    if (! mDebug)
                        printw("%s", mProgressBarDatas[idx]->getDisplayLine().c_str());
                    else
                        printf("%s\n", mProgressBarDatas[idx]->getDisplayLine().c_str());
                    refresh();
                }
            }
            pthread_mutex_unlock(&mLock);
        }
    }

    void MultilineStatus::increaseLines(int num_lines) {
        if (mMarqueeDatas.size() < num_lines) {
            mMarqueeDatas.resize(num_lines);
            mProgressBarDatas.resize(num_lines);
        }
    }

    void MultilineStatus::gotoLine(int n) {
        if (! mDebug)
            move(n, 0);
    }
}
