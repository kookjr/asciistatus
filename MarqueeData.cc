/*
 * AsciiStatus -- Dynamic progress displays for an ASCII terminal
 * 
 * Copyright (C) 2009, 2010 Mathew Cucuzella (kookjr@gmail.com)
 *
 * This file is part of AsciiStatus.
 *
 * AsciiStatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AsciiStatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AsciiStatus.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>
#include <iomanip>

#include "MarqueeData.h"

namespace mls {
    MarqueeData::MarqueeData(const std::string& title, int width, bool add_title_cnt) :
        mTitle(title),
        mWidth(width),
        mElements(),
        mAddTitleCount(add_title_cnt)
    {
    }

    MarqueeData::~MarqueeData() {
    }

    void MarqueeData::addElement(const std::string& el, int pos) {
        if (pos == 0) {
            mElements.push_front(el);
        }
        else {
            mElements.push_back(el);
        }
    }

    void MarqueeData::remElement(int pos) {
        if (mElements.size() > 0) {
            if (pos == 0) {
                mElements.pop_front();
            }
            else {
                mElements.pop_back();
            }
        }
    }

    void MarqueeData::remElement(const std::string& el) {
        std::deque<std::string>::iterator it = mElements.begin();
        while (it != mElements.end()) {
            if (el == *it) {
                mElements.erase(it);
                break;
            }
            it++;
        }
    }

    std::string MarqueeData::getDisplayLine(void) {
        int max_names = mWidth - mTitle.size() - 2;  // -2= : and space
        if (mAddTitleCount)
            max_names -= 3;                          // -3= - and 2 digit count

        std::string buffer;
        for (int i=0; i < mElements.size(); i++) {
            if (i == 0) {
                buffer.append(mElements[i]);
            }
            else {
                buffer.append(" ");
                buffer.append(mElements[i]);
            }
        }

        std::ostringstream os;
        os << mTitle;
        if (mAddTitleCount)
            os << '-' << std::setw(2) << std::setfill('0') << mElements.size();
        os << ": " << buffer.substr(0, max_names);

        // must clear out to end of line in case something was there from last time
        int todo = mWidth - os.str().size();
        for ( ; todo > 0; todo--) {
            os << " ";
        }

        return os.str();
    }

}
