/*
 * AsciiStatus -- Dynamic progress displays for an ASCII terminal
 * 
 * Copyright (C) 2009 Mathew Cucuzella (kookjr@gmail.com)
 *
 * This file is part of AsciiStatus.
 *
 * AsciiStatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AsciiStatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AsciiStatus.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MARQUEE_H
#define MARQUEE_H

#include <string>
#include "MarqueeData.h"

namespace mls {

    class Marquee {
    public:
        Marquee(void);
        ~Marquee(void);

        void update(const MarqueeData& data);

    private:
        MarqueeData mData;
    };
}

#endif /* MARQUEE_H */

/* Local Variables: */
/* mode: C++ */
/* End: */
