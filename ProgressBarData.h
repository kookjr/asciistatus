/*
 * AsciiStatus -- Dynamic progress displays for an ASCII terminal
 * 
 * Copyright (C) 2009 Mathew Cucuzella (kookjr@gmail.com)
 *
 * This file is part of AsciiStatus.
 *
 * AsciiStatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AsciiStatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AsciiStatus.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROGRESSBARDATA_H
#define PROGRESSBARDATA_H

#include <string>

namespace mls {

    class ProgressBarData {
    public:
        ProgressBarData(const std::string& title, int width);
        ~ProgressBarData(void);

        bool percentChange(double percent);
        void setSource(const std::string& source);
        void setProgress(double percent);
        std::string getDisplayLine(void);

    private:
        std::string mTitle;
        std::string mSource;
        std::string mBuffer;
        int mWidth;
        int mMaxSourceWidth;
        double mPercent;
    };

}

#endif /* MARQUEEDATA_H */

/* Local Variables: */
/* mode: C++ */
/* End: */
